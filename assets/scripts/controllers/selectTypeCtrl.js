editorApp.controller('SelectTypeCtrl', function($scope, dataService, $location) {

    $scope.setTypeToRectangular = function(isRectangular) {
        window.setRectangularMagnetsType(isRectangular);
        $location.url('select');
    }

});
