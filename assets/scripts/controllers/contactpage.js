
editorApp.controller('ContactPageCtrl', function($scope, $location, $http) {

    $scope.error = false;
    $scope.loading = false;
    $scope.success = false;

    $scope.name = '';
    $scope.email = '';
    $scope.notes = '';

    $scope.submit = function() {
        //save Order
        $scope.loading = true;

        var data = $.param({
            name: $scope.name,
            email: $scope.email,
            notes: $scope.notes
        });

        $http({
            method: 'POST',
            url: "/contactForm.php",
            data: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        }).success(function(data, status, headers, config) {

            if(data.error) {
                $scope.error = true;
                $scope.loading = false;
                $scope.errorStatus = 'Error';
                $scope.errorDescription = data.message;
            } else {
                $scope.loading = false;
                $scope.success = true;

                $scope.name = '';
                $scope.email = '';
                $scope.notes = '';
            }

        }).error(function(data, status, headers, config) {
            $scope.error = true;
            $scope.loading = false;
            $scope.errorStatus = status;
            //TODO make error description dependint ot status with adequate descriptions
            $scope.errorDescription = "Something went wrong";

        });
    }

    $scope.closeSuccess = function() {
        $scope.success = false;
    }

    $scope.closeError = function() {
        $scope.error = false;
    }
});
