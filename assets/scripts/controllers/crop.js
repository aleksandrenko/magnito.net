'use strict';

editorApp.controller('CropCtrl', function($scope, dataService, $location) {
    $scope.selectedSource = dataService.currentImageUrl;

    //if selectedSource is '' go to selectiong step
    if($scope.selectedSource == '') {
        $location.url('select');
        return false;
    }

    //waiting to load the img
    $('imageholder img').bind('load', imgLoaded);

    function imgLoaded() {
        //remove the listener or the function will fire after size are changed
        $('imageholder img').unbind('load', imgLoaded);

        var endW = settings.width;
        var endH = settings.height;
        var imgW = $('imageholder img').width();
        var imgH = $('imageholder img').height();
        var safeMargin = 10;

        // set initial size
        if(imgW > imgH) {
            $('imageholder img').attr('height', endH + safeMargin);
        } else {
            $('imageholder img').attr('width', endW + safeMargin);
        }

        // make it resizable
        $('imageholder img').resizable({
            handles: "n, e, s, w, ne, nw, sw, se",
            ghost: false,
            aspectRatio: true,
            minHeight: endW + safeMargin,
            minWidth: endH + safeMargin,
            maxWidth: imgW,
            maxHeight: imgH,
            helper: false,
            stop: function() {
                constrainImageSourceToPhotoFrame();
            }
        });

        $('.ui-wrapper').draggable({
            stop: function() {
                constrainImageSourceToPhotoFrame();
            }
        });

        // center photo jquery style
        $('.ui-wrapper').position({
            my: "center middle",
            at: "center middle",
            of: "photoframe"
        }).show();
        // end center photo

    }

    //
    // Constains the image dragable and resizable image to the photo frame
    //
    function constrainImageSourceToPhotoFrame() {
        var $draggableImage = $('.ui-draggable');
        var $photoFrame = $('photoframe');
        var safeMargin = 10 / 2;

        var endTop = $draggableImage.position().top;
        var endLeft = $draggableImage.position().left;

        //space from top
        if( $draggableImage.position().top > ( $photoFrame.position().top - safeMargin ) ) {
            endTop = $photoFrame.position().top - safeMargin;
        }

        //space from left
        if( $draggableImage.position().left > ( $photoFrame.position().left - safeMargin ) ) {
            endLeft = $photoFrame.position().left - safeMargin;
        }

        //space from right
        if( $draggableImage.position().left + $draggableImage.width() < $photoFrame.position().left + $photoFrame.width() + safeMargin) {
            endLeft = $photoFrame.position().left + $photoFrame.width() - $draggableImage.width() + safeMargin;
        }

        //space from bottom
        if( $draggableImage.position().top + $draggableImage.height() < $photoFrame.position().top + $photoFrame.width() + safeMargin) {
            endTop = $photoFrame.position().top + $photoFrame.height() - $draggableImage.height() + safeMargin;
        }

        //move it
        $draggableImage.animate({
            top: endTop,
            left: endLeft
        }, 200);
    }

    function getPosition() {
        var imgTop = $('.ui-wrapper').position().top;
        var imgLeft = $('.ui-wrapper').position().left;

        var frameTop = $('photoframe').position().top;
        var frameLeft = $('photoframe').position().left;

        //for safe cutting
        var border = 5;

        var top = Math.abs(imgTop - frameTop + border);
        var left = Math.abs(imgLeft - frameLeft + border);

        return {'top': top, 'left': left};
    }

    /* duplicating code is baaad */
    $scope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
    /* duplicating code is baaad */


    $scope.gotoEffects = function() {

        var selectedPosiion = getPosition();
        var x = selectedPosiion.left;
        var y = selectedPosiion.top;

        //for safe cutting
        var border = 5;

        var endW = settings.width + border*2;
        var endH = settings.height + border*2;

        //resize ===================================================
        var fw = $('imageholder img').width();
        var fh = $('imageholder img').height();

        var coef = 1;

        if(fw > fh) {
            coef = endH / fh;
        } else if (fw < fh) {
            coef = endW / fw
        }

        var nw = fw;// * coef;
        var nh = fh;// * coef;

        $('#offscreen').html('<canvas id="tempResizeCanvas" width="' + nw + '" height="' + nh + '"></canvas>');

        var canvasResize = $("#tempResizeCanvas").get(0);
        var ctx = canvasResize.getContext("2d");
        var img = $('imageholder img').get(0);

        ctx.drawImage(img, 0, 0, nw, nh);
        // end of resize ===================================================

        //console.log('x: ' + x);
        //console.log('y: ' + y);

        $('#offscreen').append('<canvas id="tempCropedCanvas" width="' + endW + '" height="' + endH + '"></canvas>');
        var canvasCroped = $("#tempCropedCanvas").get(0);
        var ctxCroped = canvasCroped.getContext("2d");

        //context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
        ctxCroped.drawImage(canvasResize, -x, -y, nw, fh);

        //remove canvas for resizing
        $("#tempResizeCanvas").remove();

        $location.url('filters');
    }
});
