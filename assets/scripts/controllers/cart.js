
editorApp.controller('CartCtrl', function($scope, dataService, $location) {

    if(window._gaq) {
        window._gaq.push(['_cart']);
    }

    /* duplicating code is baaad */
    $scope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
    /* duplicating code is baaad */


    if(dataService.getCartPacksNumber() < 1) {
        $location.url('select');
        return false;
    }

    $scope.theDeliveryIsFree = function() {
        return $scope.price > 50;
    };

    $scope.cart = dataService.getCart();
    $scope.totalPacks = dataService.getCartPacksNumber();
    $scope.totalPrice = dataService.getTotalPriceOfCart();
    $scope.price = dataService.getPriceOfCart();

    $scope.$watch(dataService.getTotalPriceOfCart, function() {
        $scope.totalPrice = dataService.getTotalPriceOfCart();
        $scope.totalPacks = dataService.getCartPacksNumber();
        $scope.price = dataService.getPriceOfCart();
    });

    $scope.editPack = function(id) {
        dataService.editPackFromCart(id);
        $location.url('filters');
    };

    $scope.deletePack = function(id) {
        dataService.deletePackFromCart(id);
        $scope.safeApply();
    };

    $scope.changeOrderNumber = function(packid, count) {
        var pack = dataService.getPackFromCartById(packid);

        if(pack) {
            pack.amound = count;
        }
    };

});
