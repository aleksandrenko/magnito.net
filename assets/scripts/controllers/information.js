'use strict';

editorApp.controller('InformationCtrl', function($scope, dataService, $location, $http) {

    if(window._gaq) {
        window._gaq.push(['_Information view']);
    }

    if(dataService.getCartPacksNumber() == 0) {
        $location.url('select');
    }

    $scope.information = dataService.getOrderInformation();
    $scope.loading = false;
    $scope.errorStatus = '';

    //TODO get this from a server side script when order is saved successful

    $scope.totalPrice = dataService.getTotalPriceOfCart();
    $scope.packsCount = dataService.getCartPacksNumber();

    $scope.closeError = function() {
        $scope.errorStatus = '';
    };

    $scope.theDeliveryIsFree = function() {
        return dataService.getPriceOfCart() > 50;
    };

    $scope.submitOrder = function() {
        //save Order
        $scope.loading = true;

        var fullname =  $scope.information.name;
        var email =     $scope.information.email;
        var address =   $scope.information.address;
        var phone =     $scope.information.phone;
        var notes =     $scope.information.notes;
        var facebook =  $scope.information.facebookId;
        var packs = [];

        var cart = dataService.getCart();
        angular.forEach(cart, function(pack, index) {
            packs.push({
                canvas_id: pack.canvas_id,
                amount: pack.amound
            });
        });

        var data = $.param({
            fullname: fullname,
            email: email,
            address: address,
            phone: phone,
            notes: notes,
            facebook: facebook,
            packs: packs,
            referrer: document.orderReferrer || ''
        });

        $scope.isUploadingCanvases = dataService.getOrderInformation().isUploadingCanvases;

        $scope.loading = true;

        if($scope.information.isUploadingCanvases) {
            //$scope.loading = true;
            createTheOrder();
        } else {
            //$scope.loading = true;

            $scope.$watch('isUploadingCanvases', function(newValue, oldValue) {
                //$scope.loading = false;
                createTheOrder();
            });
        }

        function createTheOrder() {
            $http({
                method: 'POST',
                url: "/_admin/create_order",
                data: data,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                }
            }).success(function(data, status, headers, config) {
                $scope.loading = false;

                if(data.error == false) {
                    $scope.information.orderNumber = data.id;
                    $location.url('complete');
                } else {
                    $scope.errorStatus = 'Error';
                    $scope.errorDescription = data.message;
                }

            }).error(function(data, status, headers, config) {
                $scope.errorStatus = status;
                //TODO make error description dependint ot status with adequate descriptions
                $scope.errorDescription = "Something went wrong";
            });
        }

    };

});
