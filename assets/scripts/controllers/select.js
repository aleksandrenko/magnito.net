'use strict';

editorApp.controller('SelectCtrl', function($scope, dataService, $location, facebookService, $http) {

    if(window._gaq) {
        window._gaq.push(['_select']);
    }

    $scope.selectedSource = '';
    $scope.loading = false;
    $scope.errorStatus = '';
    $scope.loadingFacebookPhotos = false;

    $scope.currentFacebookAlbum = 'Всички албуми';
    $scope.facebookAlbumSelected = false;

    /* duplicating code is baaad */
    $scope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
    /* duplicating code is baaad, but fast to produce :/ */


    $scope.selectResourceProvider = function(provider) {

        if(!$scope.loading) {
            $scope.selectedSource = provider;

            if(provider === 'gallery') {
                $scope.getMagnitoAlbum();
            }

            if(provider === 'facebook') {
                $scope.getFacebookAlbums();
            }
        }
    };

    $scope.closeError = function() {
        $scope.errorStatus = '';
        $scope.loading = false;
        $scope.errorDescription = '';
    };

    $scope.getPhotoFromUrl = function(url) {
        var urlData = '';

        if(url) {
            urlData = url;
        } else {
            var urlData = $scope.resourceUrl;
        }

        $scope.loading = true;
        $scope.safeApply();


        $http({
            method: 'POST',
            url: "remotecopy.php",
            data: 'url=' + urlData,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
        }).success(function(data, status, headers, config) {
            $scope.loading = false;
            dataService.currentImageUrl = data.url;
            gotoCrop();

        }).error(function(data, status, headers, config) {
            $scope.errorStatus = status;
            //TODO make error description dependint ot status with adequate descriptions
            $scope.errorDescription = "Something not found";
        });
    };

    //
    // Gallery
    //

    $scope.getMagnitoAlbum = function() {

        var html = '<magnetsgallery><galleryfilters><filter data-class="all" class="active">Всички</filter><filter data-class="sign">Надписи</filter><filter data-class="animal">Животни</filter><filter data-class="movie">Филми</filter><filter data-class="logo">Лога</filter><filter data-class="holiday">Празници</filter><filter data-class="it">Компютърни</filter><filter data-class="draw">Рисувани</filter><filter data-class="food">Храна</filter></galleryfilters>';
        html += '<content><ul>';
        html += '<li><img src="/gallery/magnets/caloriessmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign"></li><li><img src="/gallery/magnets/catcooksmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="animal"></li><li><img src="/gallery/magnets/donutssmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign"></li><li><img src="/gallery/magnets/exercisesmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign draw"></li><li><img src="/gallery/magnets/kittenssmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="animal"></li><li><img src="/gallery/magnets/leonssmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="animal"></li><li><img src="/gallery/magnets/lets-make-outsmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign"></li><li><img src="/gallery/magnets/simpsonssmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign draw"></li><li><img src="/gallery/magnets/small-kitty-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="animal"></li><li><img src="/gallery/magnets/tequilasmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign"></li><li><img src="/gallery/magnets/too-prittysmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign draw"></li><li><img src="/gallery/magnets/wash-your-handssmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="sign"></li><li><img src="/gallery/magnets/washdaysmall-magnit-za-hladilnik.png" alt="магнит за хладилник със снимка" class="animal"></li>';
        html += '<li><img src="/gallery/magnets/above-peope-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/adidas-magnet.png" alt="" class="logo"></li><li><img src="/gallery/magnets/android-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/android2-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/angri-birds-magnet.png" alt="" class="logo draw"></li><li><img src="/gallery/magnets/apple-logo-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/bad-for-you-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/bite-wof-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/breakfast2-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/breakfast3-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/breakfast4-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/brounlabrador-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/buzzin-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/cat-on-cycle-magnet.jpg" alt="" class="draw animal"></li><li><img src="/gallery/magnets/coca-cola-magnet.png" alt="" class="logo"></li><li><img src="/gallery/magnets/coca-cola-retro-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/coffee-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/coffee2-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/converse-magnet.png" alt="" class="logo"></li><li><img src="/gallery/magnets/cute-lab-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/desert-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/dino-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/dish-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/disney-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/dog-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/dog-nose-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/dragon-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/draw-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/draw2-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/draw3-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/easter-eggs1-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs10-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs11-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs12-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs2-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs3-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs4-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs5-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs6-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs7-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs8-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/easter-eggs9-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/eat-better-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/english-muffin-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/facebook-like-magnet.png" alt="" class="it"></li><li><img src="/gallery/magnets/fail-faster-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/firefox-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/food-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/free-hugs-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/game-magnet.png" alt="" class="it"></li><li><img src="/gallery/magnets/gmail-magnet.png" alt="" class="it logo"></li><li><img src="/gallery/magnets/good-luck-smartie-pants-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/green-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/harley-davidson-magnet.png" alt="" class="logo"></li><li><img src="/gallery/magnets/headshot-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/hello-kitty-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/html5-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/i-want-to-walk-on-the-moon-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/im-not-crazy-magnet.png" alt="" class="movie it"></li><li><img src="/gallery/magnets/ingredients-magnet.jpg" alt="" class="food draw"></li><li><img src="/gallery/magnets/inside-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/intel-magnet.png" alt="" class="it logo"></li><li><img src="/gallery/magnets/javascript-logo-magnet.jpg" alt="" class="it logo"></li><li><img src="/gallery/magnets/keep-up-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/kiss-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/lemons-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/letterings-12-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings-magnet1.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings10-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings11-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings13-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings14-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings15-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings16-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings17-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings18-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings19-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings2-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings3-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings4-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings5-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings6-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings7-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings8-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/letterings9-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/lick-that-window-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/little-fox-magnet.jpg" alt="" class="draw animal"></li><li><img src="/gallery/magnets/love-magnet.jpg" alt="" class="holiday"></li><li><img src="/gallery/magnets/mamoth-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/morning-coffee-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/morning-coffee2-magnet.jpg" alt="" class="food"></li><li><img src="/gallery/magnets/nodejs-logo-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/nothing-great-ever-came-that-easy-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/ouch-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/php-logo-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/puppy-labrador-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/puppy-magnet.jpg" alt="" class="animal"></li><li><img src="/gallery/magnets/red-bear-magnet.jpg" alt="" class="draw animal"></li><li><img src="/gallery/magnets/ruby-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/sandwitch-magnet.jpg" alt="" id="food"></li><li><img src="/gallery/magnets/sheldon-cooper-quotes-magnet.png" alt="" class="movie"></li><li><img src="/gallery/magnets/size-is-not-important-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/smart-is-the-new-sexy-magnet.png" alt="" class="draw movie"></li><li><img src="/gallery/magnets/stop-global-whining-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/success-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/sun-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/the-flash-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/time-for-coffee-magnet.jpg" alt="" class="sign"></li><li><img src="/gallery/magnets/tv-the-big-bang-theory-peni-magnet.png" alt="" class="movie"></li><li><img src="/gallery/magnets/tv-the-big-bang-theory-sheldon-magnet.jpg" alt="" class="movie"></li><li><img src="/gallery/magnets/tv-the-big-bang-theory1-magnet.png" alt="" class="movie"></li><li><img src="/gallery/magnets/tv-the-big-bang-theory2-magnet.png" alt="" class="movie"></li><li><img src="/gallery/magnets/tv-the-big-bang-theory3-magnet.png" alt="" class="movie"></li><li><img src="/gallery/magnets/upsss-magnet.jpg" alt="" class="draw"></li><li><img src="/gallery/magnets/wordpress-magnet.png" alt="" class="logo it"></li><li><img src="/gallery/magnets/workout-magnet.jpg" alt="" class="sign"></li>';
        html += '</ul></content></magnetsgallery><div class="clear"></div>';
        $('#GalleryTabContent').html(html);

        //filters
        $('#GalleryTabContent filter').click(function() {
            var className = $(this).attr('data-class');
            $('#GalleryTabContent filter.active').removeClass('active');
            $(this).addClass('active');

            $('#GalleryTabContent content li').hide();

            if(className !== 'all') {
                $('#GalleryTabContent .' + className).parent().show();
            } else {
                $('#GalleryTabContent li').show();
            }

        });

        $('#GalleryTabContent content img').click(function() {
            $(this).toggleClass('big');
        });
        //end of filters


        $('#GalleryTabContent img').on("click", function() {
            dataService.currentImageUrl = $(this).attr('src');
            gotoCrop();
            $scope.safeApply();
        });
    };

    // ===================================================================
    // Facebook
    // ===================================================================

    var facebookPhotosParent = $('#FacebookTabContent');
    var facebookPhotosElement = $('#facebookphotos');

    $scope.facebookReady = false;

    $scope.$on('facebookInitDone', function(event) {
        $scope.facebookReady = true;
        $scope.safeApply();
    });

    $scope.$on('facebookUserLoginCanceled', function(event) {
        if($scope.selectedSource === 'facebook') {
            $scope.selectedSource = '';
            $scope.loading = false;
            $scope.safeApply();
        }
    });

    //when albums arrive
    $scope.$on('facebookAlbumsLoaded', function(event, generatedHTML) {
        $scope.loading = false;
        //hide preloading icon at the bottom of the panel
        $scope.loadingFacebookPhotos = false;

        $scope.facebookAlbumSelected = false;
        $scope.currentFacebookAlbum = 'Всички албуми';

        $scope.safeApply();

        $('#facebookphotos').html(generatedHTML);
    });

    //when photo images arrive
    $scope.$on('facebookPhotosLoaded', function(event, generatedHTML) {
        $scope.loading = false;
        //hide preloading icon at the bottom of the panel
        $scope.loadingFacebookPhotos = false;
        $scope.safeApply();

        var htmlToAppend = $(generatedHTML);

        facebookPhotosElement.append(htmlToAppend).masonry('appended', htmlToAppend).masonry('reload');

        //load and hold all images in global container
        $('#facebookphotosholder').html($(facebookPhotosElement).html());

        //once new images are loaded infinity scroll is good to go/load more
        readyToLoadMomore = true;
    });

    var stepBeforLoad = 300;
    var endScrollPosition = facebookPhotosParent.height() + stepBeforLoad;
    var elementWidth = facebookPhotosElement.height();
    var readyToLoadMomore = true;
    var endOfGalleryReached = false;

    //watch the scroll and when at the bottom, load more photos
    //facebookPhotosParent.bind('scroll', scrollFunc);

    function gotoCrop() {
        $location.url('crop');
    }

    function scrollFunc(e) {

        if(!readyToLoadMomore || endOfGalleryReached) {
            return false;
        }

        elementWidth = facebookPhotosElement.height();

        if(endScrollPosition + facebookPhotosParent.scrollTop() > elementWidth) {
            //show preloading icon at the bottom of the panel
            $scope.loadingFacebookPhotos = true;
            readyToLoadMomore = false;
            $scope.safeApply();

            facebookService.getPhotos();
        }
    }


    $scope.$on('facebookPhotosEnd', function(event) {
        readyToLoadMomore = false;
        $scope.loadingFacebookPhotos = false;

        //console.log('end of load');

        endOfGalleryReached = true;
        $scope.safeApply();
    });


    //selecting photo from the loaded photos
    $('#FacebookTabContent').on('click', function(e) {
       var target = $(e.target);

       //data-src is a attr with the url of the full sized img
       var url = target.attr('data-src');

       if(url) {
           $scope.getPhotoFromUrl(url);
       }

       //when cliking on an album
       var album_id = target.attr('data-album-id');
       if(album_id) {
           var albumName = target.attr('data-album-name');

           $scope.facebookAlbumSelected = true;
           $scope.currentFacebookAlbum = albumName;

           var comming_from_album = true;

           $scope.getFaceBookPictures(album_id, comming_from_album);
       }

       return false;
    });


    $scope.getFacebookAlbums = function() {
        $scope.loading = true;

        facebookService.getAlbums();
    };

    //when facebook album is selected
    $scope.getFaceBookPictures = function(album_id, is_comming_from_album) {
        //clear html after clicking on an album

        facebookPhotosElement.html('');
        $('#facebookphotosholder').html('');

        //watch the scroll and when at the bottom, load more photos
        facebookPhotosParent.bind('scroll', scrollFunc);
        readyToLoadMomore = true;
        endOfGalleryReached = false;
        $scope.loadingFacebookPhotos = false;

        if(facebookPhotosElement.html() == '') {

            facebookPhotosElement.imagesLoaded(function() {
                facebookPhotosElement.masonry({
                    itemSelector: '.userphoto'
                });
            });

            //if there is saved html from previous looking go and get it
            if($('#facebookphotosholder').html() !== '') {
                var htmlToAppend = $('#facebookphotosholder').html();
                facebookPhotosElement.append(htmlToAppend).masonry('appended', $(htmlToAppend)).masonry('reload');
            } else {
                facebookService.getPhotos(album_id, is_comming_from_album);
                $scope.loading = true;
                $scope.safeApply();
            }
        }
    };

    // ===================================================================
    // File Upload
    // ===================================================================


    $('#fileSelectInput').on('change', function(e) {
        $scope.loading = true;
        $scope.safeApply();

        $("#imageform").ajaxForm({
            success: function(data) {
                $scope.loading = false;

                var response = angular.fromJson(data);

                if(response.error) {
                    $scope.errorStatus = 'Error';
                    $scope.errorDescription = response.message;
                } else {
                    //ITS OKEY GO GO GO
                    dataService.currentImageUrl = response.url;
                    gotoCrop();
                }

                $scope.safeApply();
            }
        }).submit();
    });


    // ===================================================================
    // Save the selected source for back
    // ===================================================================

    if(dataService.selectedService) {
        $scope.selectResourceProvider(dataService.selectedService);
    }

    $scope.$watch('selectedSource', function() {
        dataService.selectedService = $scope.selectedSource;
    });



});

