'use strict';

editorApp.controller('FiltersCtrl', function($scope, dataService, $location) {

    if(window._gaq) {
        window._gaq.push(['_filters']);
    }

    $scope.selectedCanvas = dataService.getCurrentCanvas();
    $scope.rendering = false;
    $scope.isfullpack = false;
    $scope.cartCount = 0;

    //Ако няма offscreen canvas заначи не е минато през предишната стъпка и няма изрязан канвас,
    //към който може да се прилагат филтри
    if($('#offscreen canvas').length < 1) {
        $location.url('crop');
        return false;
    }

    //this function get outside canvas and copy it on the main one
    function copyCanvas(canvasToCopy) {
        //for safer cutting
        var border = 5;

        var w = settings.width + border*2;
        var h = settings.height + border*2;

        $('photoframe').html('<canvas width="' + w + '" height="' + h + '" id="renderCanvas"></canvas>');

        var ctx = $("#renderCanvas").get(0).getContext("2d");
        ctx.drawImage(canvasToCopy, 0, 0, w, h);

        return $("#renderCanvas").get(0);
    }

    //copy selected canvas from precios step (cut)
    copyCanvas($('#offscreen canvas').get(0));


    // ==============================================================
    // render with effects
    // ==============================================================

    function renderFx(effect) {
        if($scope.rendering) {
            return false
        }

        //clean canvas with each render
        var newCopyCanvas = copyCanvas($('#offscreen canvas').get(0));
        $scope.rendering = false;

        Caman( newCopyCanvas, function () {
            // ==========================================
            // EFFECTS
            // ==========================================

            switch(effect) {
                case 'vintage':
                    //apply effect
                    this.vintage();
                    break;
                case 'lomo':
                    //apply effect
                    this.lomo();
                    break;
                case 'clarity':
                    //apply effect
                    this.clarity();
                    break;
                case 'oldboot':
                    //apply effect
                    this.oldBoot();
                    break;
                case 'nostalgia':
                    //apply effect
                    this.nostalgia();
                    break;
                case 'sunrise':
                    //apply effect
                    this.sunrise();
                    break;
                default:
                    //render with no effects
                    //renderFx();
            }

            this.render();
        });
    }

    $scope.fx = function(effect) {
        renderFx(effect);
    };


    // =========================================================
    // Rendering Message
    // =========================================================

    /* duplicating code is baaad */
    $scope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
    /* duplicating code is baaad */

    if(!dataService.isCamanListenersInitialised) {
        dataService.isCamanListenersInitialised = true;

        Caman.Event.listen("renderFinished", function () {
            $scope.rendering = false;
            $scope.safeApply();
        });

        // Listen to all CamanJS instances
        Caman.Event.listen("processStart", function () {
            $scope.rendering = true;
            $scope.safeApply();
        });
    }


    // =========================================================
    // Render Borders
    // =========================================================

    $scope.draw = function(e, domSelect) {
        //for safer cutting
        var border = 5;

        var w = settings.width + border*2;
        var h = settings.height + border*2;

        $('photoborder').html('<canvas width="' + w + '" height="' + h + '"></canvas>');

        var clickTarget = '';

        if(e !== null) {
            clickTarget = $(e.target)[0];
        }

        if(domSelect !== undefined) {
            clickTarget = $(domSelect).get(0);
        }

        if($(clickTarget).hasClass('frame')) {
            var ctx = $('photoborder canvas').get(0).getContext('2d');
            ctx.drawImage(clickTarget, 0, 0);
        }

    };

    //default frame
    //$scope.draw(null, '#defaultFrame');

    // =========================================================
    // Add Canvas with effects and borders to pack
    // =========================================================

    $scope.addToPack = function() {
        var pack = dataService.getCurrentPack();

        //check if there are too much magnitos in this pack
        if(dataService.getCurrentPackLength() > 8) return false;

        var border = 5;

        var w = settings.width + border*2;
        var h = settings.height + border*2;

        // if border exist copy it on photoframe canvas
        var newcanvas = '<canvas width="' + w + '" height="' + h + '" id="mergedCanvas"></canvas>';
        $('#temp').html(newcanvas);
        var ctx = $('#mergedCanvas').get(0).getContext('2d');

        var magnito = $('photoframe canvas').get(0);
        ctx.drawImage(magnito, 0, 0);

        var border = $('photoborder canvas').get(0);
        if(border) {
            ctx.drawImage(border, 0, 0);
        }

        //add canvas to pack ==================================
        var newMagnito = dataService.createNewMagnito($('#mergedCanvas').get(0));
        dataService.addMagnitoToCurrentPack(newMagnito);
        // end of add canvas to pack ==========================
    }


});
