
editorApp.controller('FacebookCtrl', function($scope, dataService, facebookService, $location) {

    $scope.fbuser = facebookService.getUser();
    $scope.username = '';
    $scope.avatar = '';

    $scope.$on('facebookInitDone', function(event, user) {
        $scope.facebookReady = true;
        $scope.$apply();
    });

    $scope.$on('facebookUserLoaded', function(event, user) {
        // assign the loaded track as the 'current'

        $scope.fbuser = facebookService.getUser();
        $scope.avatar = 'https://graph.facebook.com/' + $scope.fbuser.id  + '/picture';

        $scope.$apply();
    });

    $scope.$on('facebookUserLogout', function(event, user) {
        $scope.avatar = '';
        $scope.fbuser = '';
        $scope.$apply();
    });

    $scope.facebookLogOut = function() {
        facebookService.logout();
    };

    $scope.facebookLogin = function() {
        facebookService.login();
    };


});
