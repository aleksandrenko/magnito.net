
editorApp.controller('OrderCompleteCtrl', function($scope, dataService, $location) {

    if(window._gaq) {
        window._gaq.push(['_orderComplete']);
    }

    if(dataService.getCartPacksNumber() == 0) {
        $location.url('select');
    }

    $scope.theDeliveryIsFree = function() {
        return dataService.getPriceOfCart() > 50;
    };

    $scope.information = dataService.getOrderInformation();
    $scope.packsCount = dataService.getCartPacksNumber();
    $scope.totalPrice = dataService.getTotalPriceOfCart();
});
