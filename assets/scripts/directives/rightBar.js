
editorApp.directive('rightbar' ,function(dataService, $location) {
    return {
        templateUrl: '/assets/scripts/directives/views/rightBar.html',
        restrict: 'E',
        scope: {},
        link: function postLink(scope, element, attrs) {
            scope.isfullpack = 0;
            scope.cartCount = 0;

            scope.$watch(dataService.getCurrentPackLength, function() {
                drawPack();
            });

            function drawPack() {
                scope.isfullpack = dataService.getCurrentPackLength() === 9;
                scope.cartCount = dataService.getCartPacksNumber();

                var $magnitos = $(element).find('magnit');
                //clear for new rendering
                $magnitos.find('canvas').remove();
                //this removes all dels and replace them with adds, it's not optimal but there no need to be :)
                $('del').parent().append('<add></add>');
                $('del').remove();

                var pack = dataService.getCurrentPack();

                var smallCanvasWidth = 100;
                var smallCanvasHeight = settings.rectangularMagnetsType ? 77 : 100;

                $.each( pack.data, function(packIndex, data) {

                    if(data !== null) {
                        //cycle all magnito dom elements
                        $.each( $magnitos, function( index, value ) {
                            //if there is no canvas in the current div
                            if( $(value).find("canvas").length < 1 ) {
                                $(value).html('<canvas width="' + smallCanvasWidth + '" height="' + smallCanvasHeight + '" data-id="' + data.id + '"></canvas><del></del>');
                                var ctx = $(value).find("canvas").get(0).getContext("2d");
                                ctx.drawImage(data.canvas, 0, 0, smallCanvasWidth, smallCanvasHeight);

                                return false;
                            }
                        });
                    }
                });
            }

            //when user click on a magnet in the pack
            scope.magnitAction = function(e) {
                var clickTarget = $(e.srcElement)[0] || $(e.target);

                if( $(clickTarget).is('del') ) {
                    //delete clicked
                    var id = $(clickTarget).parent().find('canvas').attr('data-id');
                    scope.removeFromPack(id);
                } else {
                    $location.url('select');
                }
            };

            scope.removeFromPack = function(id) {
                dataService.deteleMagnitoFromCurrentPack(id);
                drawPack();
            };

            scope.addToCart = function() {
                dataService.addCurrentPackToCart();

                //create new pack and make it current
                var newpack = dataService.newPack();
                drawPack();
            };


        }
    };
});
