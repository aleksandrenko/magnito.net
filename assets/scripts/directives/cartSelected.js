
editorApp.directive('cartselected', function() {
    return {
        templateUrl: '/assets/scripts/directives/views/cartSelected.html',
        restrict: 'E',
        scope: {
            pack: '=pack'
        },
        link: function postLink(scope, element, attrs) {
            scope.orderedNumberOfThisPack = scope.pack.amound;
            var canvases = $(element).find('canvas');

            var smallCanvasWidth = 100;
            var smallCanvasHeight = settings.rectangularMagnetsType ? 77 : 100;

            $.each( scope.pack.data, function(packIndex, magnitData) {
                var ctx = $(element).find('canvas').get(packIndex).getContext("2d");
                ctx.drawImage(magnitData.canvas, 0, 0, smallCanvasWidth, smallCanvasHeight);
            });

            scope.editPack = function () {
                scope.$parent.editPack(scope.pack.id);
            };

            scope.deletePack = function () {
                scope.$parent.deletePack(scope.pack.id);
            };

            scope.changeOrderNumber = function() {
                var count = scope.orderedNumberOfThisPack;
                scope.$parent.changeOrderNumber(scope.pack.id, count);
            }
        }
    };
});

