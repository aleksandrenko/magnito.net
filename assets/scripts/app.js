'use strict';

window.setRectangularMagnetsType = function(isRectangular) {
    window.settings = {
        rectangularMagnetsType: isRectangular
    };

    if(settings.rectangularMagnetsType) {
        //rectangular magnets
        settings.width = 530;
        settings.height = 410;
        $('body').addClass("rectangular-magnets");
    } else {
        //square magnets
        settings.width = 400;
        settings.height = 400;
        $('body').removeClass("rectangular-magnets");
    }
};
window.setRectangularMagnetsType(false);

var editorApp = angular.module('editorApp', []);

editorApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/selectType', {
            templateUrl: '/assets/views/selectType.html',
            controller: 'SelectTypeCtrl'
        })
        .when('/select', {
            templateUrl: '/assets/views/select.html',
            controller: 'SelectCtrl'
        })
        .when('/crop', {
            templateUrl: '/assets/views/crop.html',
            controller: 'CropCtrl'
        })
        .when('/filters', {
            templateUrl: '/assets/views/filters.html',
            controller: 'FiltersCtrl'
        })
        .when('/cart', {
            templateUrl: '/assets/views/cart.html',
            controller: 'CartCtrl'
        })
        .when('/order', {
            templateUrl: '/assets/views/order.html',
            controller: 'InformationCtrl'
        })
        .when('/complete', {
            templateUrl: '/assets/views/complete.html',
            controller: 'OrderCompleteCtrl'
        })
        .otherwise({
            redirectTo: '/select'
        });
    }]);


editorApp.factory('uniuqueIdService', function() {
    var service = {};

    var uniqueMagnetId = 0;
    var uniquePackId = 0;

    service.uniquePackID = function() {
        return uniquePackId++;
    };

    service.uniqueMagnitoID = function() {
        return uniqueMagnetId++;
    };

    return service;
});



var FB_ACCESS_TOKEN = '';

editorApp.factory('facebookService', function($rootScope, dataService) {
    var service = {};

    var API = {};
    var FB_USER = {};
    var status = '';
    var isCamanListenersInitialised = false;

    window.fbAsyncInit = function() {
        API = FB;

        API.init({
            appId      : '460342137362549', // App ID from the App Dashboard
            //channelUrl : '//www.magnito.net/channel.html', // Channel File for x-domain communication
            status     : true, // check the login status upon init?
            cookie     : true, // set sessions cookies to allow your server to access the session?
            xfbml      : true  // parse XFBML tags on this page?
        });

        API.getLoginStatus(function(response) {

            //console.log(response.status);

            $rootScope.$broadcast('facebookInitDone');

            if (response.status === 'connected') {
                getUser();
                status = 'connected';

                FB_ACCESS_TOKEN =   FB.getAuthResponse()['accessToken'];
                // connected just fine don't do nothing
            } else if (response.status === 'not_authorized') {
                // not_authorized
                status = 'not_authorized';
                //console.log('not_authorized');
            } else {
                // not_logged_in
                status = 'not_logged_in';
                //console.log('not_logged_in');
            }
        });
    };

    service.logout = function(successFunc) {
        API.logout(function(response) {
            // user is now logged out
            FB_ACCESS_TOKEN = '';
            var FB_USER = {};

            status = 'not_logged_in';

            var userOrderData = dataService.orderinformation;

            userOrderData.name = '';
            userOrderData.email = '';
            userOrderData.facebookpage = '';
            userOrderData.facebookId = '';
            $rootScope.$broadcast('facebookUserLogout', FB_USER);
        });
    };

    service.login = function(successFunc) {

        //if there is no init done yet, no internet or delay
        if(status === '') {
            return false;
        }

        API.login(function(response) {

            if (response.authResponse) {
                getUser();
                FB_ACCESS_TOKEN =   FB.getAuthResponse()['accessToken'];
                status = 'connected';

                if(successFunc) {
                    successFunc();
                }
            } else {
                $rootScope.$broadcast('facebookUserLoginCanceled');
            }
        }, {scope: 'email,user_photos,user_status'});
    };

    service.getUser = function() {
        return FB_USER;
    };

    function getUser() {
        API.api('/me', function(response) {
            //console.log(response);
            FB_USER = response;

            var userOrderData = dataService.orderinformation;

            userOrderData.name = response.name;
            userOrderData.email = response.email;
            userOrderData.facebookpage = response.link;
            userOrderData.facebookId = response.id;

            $rootScope.$broadcast('facebookUserLoaded', FB_USER);
        });
    }


    service.getAlbums = function() {

        var albumHtmlHolder = $('#facebookalbumholder');

        if(albumHtmlHolder.html() != '') {
            var generatedHTML = albumHtmlHolder.html();
            $rootScope.$broadcast('facebookAlbumsLoaded', generatedHTML);
            return false
        }

        //reset selected album so paging will be ok
        selectedAlbumId = '';

        if(status != 'connected') {
            service.login(getAlbums);
            return false;
        }

        //допълнителна функция, която може да се изпрати към логин-а и да се изпълни след
        //логин-а
        function getAlbums() {

            var fb_request = '/me?fields=albums.limit(100).fields(cover_photo,name,count)';

            API.api(fb_request, function(response) {
                var generatedHTML = '';

                if(response.albums.data.length > 0) {
                    var i = 0;
                    var length = response.albums.data.length;

                    for(i; i < length; i++) {
                        var album = response.albums.data[i];
                        //console.log(album);

                        var imgSrc = '<img src="https://graph.facebook.com/' + album.cover_photo + '/picture?type=normal&access_token=' + FB_ACCESS_TOKEN + '" data-album-id="' + album.id + '" data-album-name="' + album.name + '" />';
                        generatedHTML += '<a class="facebook_album" href="#" data-album-id="' + album.id + '" data-album-name="' + album.name + '" ><cover_photo data-album-id="' + album.id + '" data-album-name="' + album.name + '" >' + imgSrc + '</cover_photo> ' + album.name + ' (' + album.count + ')</a>';
                    }
                };

                albumHtmlHolder.html(generatedHTML);
                $rootScope.$broadcast('facebookAlbumsLoaded', generatedHTML);
            });
        };
        getAlbums();
    };

    var nextPageUrl = '';
    var endOfPaging = false;
    var selectedAlbumId = '';

    service.getPhotos = function(album_id, is_comming_from_album) {

        if(!album_id) {
            album_id = selectedAlbumId;
        }

        //if different album id is selected remove paging and select the new album
        if(selectedAlbumId !== album_id || selectedAlbumId === '' || is_comming_from_album) {
            nextPageUrl = '';
            selectedAlbumId = album_id;
        }

        if(status != 'connected') {
            service.login(getPhotos);
            return false;
        }

        //Функция е за да може да се вика след логин
        function getPhotos() {

            var fb_request = '/' + album_id + '?fields=photos.limit(25).fields(images,source)';

            if(nextPageUrl !== '' || nextPageUrl === undefined) {
                getPhotosPaging(nextPageUrl);

                if(nextPageUrl === undefined) {
                    $rootScope.$broadcast('facebookPhotosEnd');
                }
            } else {
                API.api(fb_request, function(response) {

                    if(response.photos.paging == undefined) {
                        endOfPaging = true;
                        $rootScope.$broadcast('facebookPhotosEnd');
                    } else {
                        nextPageUrl = response.photos.paging.next;
                    }

                    var generatedHTML = photosCreateHTML(response.photos.data);

                    $rootScope.$broadcast('facebookPhotosLoaded', generatedHTML);
                });
            }

        }
        getPhotos();

        function getPhotosPaging(url) {
            if(endOfPaging) return false;

            $.ajax({
                url: url,
                dataType: 'json',
                context: document.body
            }).done(function(json) {

              if(json.paging === undefined) {
                  endOfPaging = true;
                  $rootScope.$broadcast('facebookPhotosEnd');
              } else {
                  nextPageUrl = json.paging.next;
              }

              var generatedHTML = photosCreateHTML(json.data);
              $rootScope.$broadcast('facebookPhotosLoaded', generatedHTML);
        });
        }
    };

    function photosCreateHTML(fb_photos) {

        var photos = fb_photos;
        var html = '';

        var requiredW = 200;
        var currentH = 0;
        var photoIndex = 5;
        var minHeight = settings.height;
        var minWidth = settings.width;

        for(var i=0; photos.length > i; i++) {

            if(photos[i].images.length < 6) {
                photoIndex = photos[i].images.length - 1;
            }

            var source = photos[i].images[0].source;
            // optimize source for size
            if(photos[i].images[3] && photos[i].images[3].height > minHeight && photos[i].images[3].width > minWidth) {
                // if img3 is good size set it as source
                source = photos[i].images[3].source;
            } else if(photos[i].images[2] && photos[i].images[2].height > minHeight && photos[i].images[2].width > minWidth) {
                // else if img2 is good size set it as source
                source = photos[i].images[2].source;
            } else if(photos[i].images[1].height > minHeight && photos[i].images[1].width > minWidth) {
                // else if img1 is good size set it as source
                source = photos[i].images[1].source;
            }

            currentH = (photos[i].images[photoIndex].height * requiredW) / photos[i].images[photoIndex].width;
            html += '<div class="userphoto" style="height:' + currentH + 'px;"><img data-src="' + source + '" src="' + photos[i].images[photoIndex].source + '" width="' + requiredW + '" height="' + currentH + '" /></div>';
        }

        return html;
    }

    return service;
});


editorApp.factory('dataService', function(uniuqueIdService, $http) //noinspection UnterminatedStatementJS
{
    var selectedService = '';
    var service = {};

    var pricePerPack = 19.99;
    var pricePerLargePack = 24.99;
    var priceCurrency = 'лв.';

    // array full of packages
    var cart = [];

    //create new pack for magnitos
    var currentPack = {};

    var isUploadingCanvases = false;

    var orderinformation = {};
        orderinformation.name = '';
        orderinformation.email = '';
        orderinformation.phone = '';
        orderinformation.address = '';
        orderinformation.notes = '';
        orderinformation.facebookpage = '';
        orderinformation.facebookId = '';
        orderinformation.orderNumber = '';

    service.orderinformation = orderinformation;

    service.getOrderInformation = function() {
        return orderinformation;
    };

    service.newPack = function() {
        var newpack = {};

        newpack.id = uniuqueIdService.uniquePackID();
        newpack.canvas_id = '';
        newpack.data = [];
        newpack.amound = 1;

        return currentPack = newpack;
    };

    //create new pack on init
    service.newPack();

    service.getCurrentPack = function() {
        return currentPack;
    };

    service.deletePackFromCart = function(id) {
        angular.forEach(cart, function(pack, index) {
            if(pack.id == id) {
                cart.splice(index, 1);
            }
        });
    };

    //this removes the pack from the cart and set it as current pack
    service.editPackFromCart = function(id) {

        angular.forEach(cart, function(pack, index) {
            if(pack.id == id) {
                currentPack = pack;
                cart.splice(index, 1);
            }
        });
    };

    service.addCurrentPackToCart = function() {
        var packInCart = false;

        angular.forEach(cart, function(pack, index) {
            if(pack.id == currentPack.id) {
                packInCart = true;
            }
        });

        if(!packInCart) {
            cart.push(currentPack);
            savePackAsCanvasToTheServer(currentPack);
        }

        function savePackAsCanvasToTheServer(currentPack) {
            var dataToSend = generateBigCanvas(currentPack);

            isUploadingCanvases = true;

            $http({
                method: 'POST',
                url: "print.php",
                data: dataToSend,
                headers: {
                    "Content-Type": "application/upload; charset=UTF-8"
                }
            }).success(function(data, status, headers, config) {
                if(data.error) {
                    //error, die silent
                } else {
                    currentPack.canvas_id = data.id;
                    isUploadingCanvases = false;
                }

            }).error(function(data, status, headers, config) {
                $scope.errorStatus = status;
                $scope.errorDescription = "Error while trying to save selected images in order.";
            });
        }

        function generateBigCanvas(pack) {
            var border = 5;
            var paddingX = 10;
            var paddingY = 30;
            var canvasWidth = settings.width*3 + paddingX*3 + border*6;
            var canvasHeight = settings.height*3 + paddingY*3 + border*6;;

            $('#bigcanvasholder').html('<canvas id="bigcanvas" width="' + canvasWidth + '" height="' + canvasHeight + '"></canvas>');
            var ctx = $("#bigcanvas").get(0).getContext("2d");

            // ==================================

            //magnito has id and canvas
            angular.forEach(pack.data, function(magnito, i) {
                var x = i%3 * settings.width + border*2;
                var y = Math.floor(i/3) * settings.height + border*2;

                x += i%3 * paddingX;
                y += Math.floor(i/3) * paddingY;

                //draw rectangular guidlines for cutting
                var ctx_tmp = magnito.canvas.getContext('2d');
                ctx_tmp.strokeStyle = '#666666';
                ctx_tmp.lineWidth = 1;
                ctx_tmp.strokeRect(border-1,border-1,settings.width+border/2,settings.height+border/2);

                ctx.drawImage(magnito.canvas, x, y);
            });

            return $('#bigcanvas').get(0).toDataURL("image/png");
        }

        //returns true if pack is added in cart
        return !packInCart;
    };


    service.getCart = function() {
        return cart;
    };

    service.getPackFromCartById = function(id) {
        var packtoreturn = null;

        angular.forEach(cart, function(pack, index) {
            if(pack.id == id) {
                packtoreturn = pack;
            }
        });

        return packtoreturn;
    };

    service.getPriceOfCart = function() {
        var totalPrice = 0;
        var _pricePerPack = settings.rectangularMagnetsType ? pricePerLargePack : pricePerPack;

        angular.forEach(cart, function(pack, index) {
            totalPrice += pack.amound * _pricePerPack
        });

        totalPrice = Math.ceil(totalPrice * 100) / 100;

        return totalPrice;
    };

    service.getTotalPriceOfCart = function() {
        return service.getPriceOfCart() + priceCurrency;
    };

    service.getCartPacksNumber = function() {
        var totalPacksNumber = 0;

        angular.forEach(cart, function(pack, index) {
            totalPacksNumber += Math.abs(pack.amound);
        });

        return totalPacksNumber;
    };

    service.createNewMagnito = function(canvas) {
        var magnito = {};

        magnito.id = uniuqueIdService.uniqueMagnitoID();
        magnito.canvas = canvas;

        return magnito;
    };

    service.getCurrentPackLength = function() {
        return currentPack.data.length;
    };

    service.addMagnitoToCurrentPack = function(magnito) {
        currentPack.data.push(magnito);
    };

    service.deteleMagnitoFromCurrentPack = function(id) {
        angular.forEach(currentPack.data, function(magnito, index) {
            if(magnito.id == id) {
                currentPack.data.splice(index, 1);
            }
        });
    };

    //just sting representing current selected image for cutting and applying filters/borders
    service.currentImageUrl = '';

    service.setCurrentCanvas = function(canvas) {
        service.currentCanvas = canvas;
    };

    service.getCurrentCanvas = function() {
        return service.currentCanvas;
    };

    //factory function body that constructs shinyNewServiceInstance
    return service;
});


