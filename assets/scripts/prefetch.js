

/* Precache images */

$(document).ready(function() {

    var preloadInterval = setInterval(function() {

        var html = '<div style="display: none;">' +
            '<img src="/assets/img/ajax-loader.gif" />' +
            '<img src="/assets/img/icon-email.png" />' +
            '<img src="/assets/img/bg-facebook.png" />' +
            '<img src="/assets/img/icon-error.png" />' +
            '<img src="/assets/img/bg-internet.png" />' +
            '<img src="/assets/img/icon-facebook-selected.png" />' +
            '<img src="/assets/img/bg-upload.png" />' +
            '<img src="/assets/img/icon-facebook.png" />' +
            '<img src="/assets/img/icon-globe-selected.png" />' +
            '<img src="/assets/img/btn-icon-cart.png" />' +
            '<img src="/assets/img/icon-globe.png" />' +
            '<img src="/assets/img/btn-icon-fb.png" />' +
            '<img src="/assets/img/icon-note.png" />' +
            '<img src="/assets/img/btn-icon-globe.png" />' +
            '<img src="/assets/img/icon-notification-success.png" />' +
            '<img src="/assets/img/btn-icon-plus.png" />' +
            '<img src="/assets/img/icon-pack.png" />' +
            '<img src="/assets/img/btn-icon-right-arrow.png" />' +
            '<img src="/assets/img/icon-person.png" />' +
            '<img src="/assets/img/cart-icon-delivery.png" />' +
            '<img src="/assets/img/icon-phone.png" />' +
            '<img src="/assets/img/cart-icon-pack.png" />' +
            '<img src="/assets/img/icon-remove.png" />' +
            '<img src="/assets/img/fb-ajax.gif" />' +
            '<img src="/assets/img/icon-shopping-cart.png" />' +
            '<img src="/assets/img/fb.png" />' +
            '<img src="/assets/img/img.png" />' +
            '<img src="/assets/img/input-error.png" />' +
            '<img src="/assets/img/framer.png" />' +
            '<img src="/assets/img/input-success.png" />' +
            '<img src="/assets/img/icon-add.png" />' +
            '<img src="/assets/img/s1-facebook-hover.png" />' +
            '<img src="/assets/img/icon-address.png" />' +
            '<img src="/assets/img/s1-facebook.png" />' +
            '<img src="/assets/img/icon-big-cart.png" />' +
            '<img src="/assets/img/s1-internet-hover.png" />' +
            '<img src="/assets/img/icon-borders.png" />' +
            '<img src="/assets/img/s1-internet.png" />' +
            '<img src="/assets/img/icon-cloud-selected.png" />' +
            '<img src="/assets/img/s1-upload-hover.png" />' +
            '<img src="/assets/img/icon-cloud.png" />' +
            '<img src="/assets/img/s1-upload.png" />' +
            '<img src="/assets/img/icon-delivery.png" />' +
            '<img src="/assets/img/filters/landscape/clarity.png" />' +
            '<img src="/assets/img/filters/landscape/lomo.png" />' +
            '<img src="/assets/img/filters/landscape/normal.png" />' +
            '<img src="/assets/img/filters/landscape/nostalgia.png" />' +
            '<img src="/assets/img/filters/landscape/oldboot.png" />' +
            '<img src="/assets/img/filters/landscape/original.jpg" />' +
            '<img src="/assets/img/filters/landscape/sunrise.png" />' +
            '<img src="/assets/img/filters/landscape/vinage.png" />' +
            '<img src="/assets/img/frames/2.png" />' +
            '<img src="/assets/img/frames/3.png" />' +
            '<img src="/assets/img/frames/4.png" />' +
            '<img src="/assets/img/frames/5.png" />' +
            '<img src="/assets/img/frames/6.png" />' +
            '<img src="/assets/img/frames/7.png" />' +
            '<img src="/assets/img/frames/sample.png" />' +
            '<img src="/assets/img/frames-rectangular/2.png" />' +
            '<img src="/assets/img/frames-rectangular/3.png" />' +
            '<img src="/assets/img/frames-rectangular/4.png" />' +
            '<img src="/assets/img/frames-rectangular/5.png" />' +
            '<img src="/assets/img/frames-rectangular/6.png" />' +
            '<img src="/assets/img/frames-rectangular/7.png" />' +
            '<img src="/assets/img/frames-rectangular/sample.png" />' +
            '</div>';

        $('body').append(html);
        window.clearInterval(preloadInterval);
        //console.log('preload');

    }, 1000);

});
