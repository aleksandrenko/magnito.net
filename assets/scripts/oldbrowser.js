var html = '<div id="browserCheck">' +
                '<div class="bg"></div>' +
                '<div class="focus">' +
                '    <div class="title">Браузърът ви изглежда остарял.</div>'+
                'Направихме magnito.net с най-новите инструменти и технологии.'+
                'Това прави magnito по-бърз и лесен за използване.'+
                'За съжаление вашият браузър не поддържа тези технологии.'+
                '    <br /><br />'+
                '    <b>Изтеглете нов, модерен браузър:</b>'+
                '    <div class="browsers">'+
                '        <div class="chrome">'+
                '            <a target="upgrade_browser" href="https://www.google.com/intl/bg/chrome/browser/">'+
                '                <div class="icon"></div><br />'+
                '            Google Chrome'+
                '            </a>'+
                '        (Chrome 7+)'+
                '        </div>'+
                '        <div class="firefox">'+
                '            <a target="upgrade_browser" href="http://www.mozilla.org/en-US/firefox/new/">'+
                '                <div class="icon"></div><br />'+
                '            Mozilla Firefox'+
                '            </a>'+
                '        (Firefox 4+)'+
                '        </div>'+
                '        <div class="safari">'+
                '            <a target="upgrade_browser" href="http://www.apple.com/safari/">'+
                '                <div class="icon"></div><br />'+
                '            Apple Safari'+
                '            </a>'+
                '        (Safari 4+)'+
                '        </div>'+
                '        <div class="ie">'+
                '            <a target="upgrade_browser" href="http://windows.microsoft.com/en-US/internet-explorer/download-ie">'+
                '                <div class="icon"></div><br />'+
                '            Internet Explorer'+
                '            </a>'+
                '        (IE 9+)'+
                '        </div>'+
                '    </div>'+
                '</div>'+
            '</div>';


document.write(html);