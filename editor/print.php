<?php

if (isset($GLOBALS["HTTP_RAW_POST_DATA"])) {
	$imageData=$GLOBALS['HTTP_RAW_POST_DATA'];

	$filteredData=substr($imageData, strpos($imageData, ",")+1);
	$unencodedData=base64_decode($filteredData);

  $filename = strtotime("now");
	$fp = fopen( 'prints/' . $filename . '.png', 'wb' );
	fwrite( $fp, $unencodedData);
	fclose( $fp );

	$arr = array('id' => $filename);
  echo json_encode($arr);

} else {
  $arr = array('error' => True, 'message' => 'Something went wrong!');
  echo json_encode($arr);
}
?>