<?php


function itg_fetch_image($img_url, $store_dir = 'uploads', $store_dir_type = 'absolute', $overwrite = false, $pref = false, $debug = false) {
    //first get the base name of the image
    $i_name = explode('.', basename($img_url));
	
	// Nyama nuzhda da se pazyat vsichki faylove, kopiraneto stava samo, za da mozhe potrebitelya da si zaredi kartinkata, taka che da mozhe da se obrabotva ot canvas (iska se da e lokalno)
	//$i_name = 'tmp_copy';
    $i_name = $i_name[0];

    //now try to guess the image type from the given url
    //it should end with a valid extension...
    //good for security too
    if(preg_match('/https?:\/\/.*\.png$/i', $img_url)) {
        $img_type = 'png';
    }
    else if(preg_match('/https?:\/\/.*\.(jpg|jpeg)$/i', $img_url)) {
        $img_type = 'jpg';
    }
    else if(preg_match('/https?:\/\/.*\.gif$/i', $img_url)) {
        $img_type = 'gif';
    }
    else {
        if(true == $debug)
            echo 'Invalid image URL';
        return ''; //possible error on the image URL
    }

    $dir_name = (($store_dir_type == 'relative')? './' : '') . rtrim($store_dir, '/') . '/';

    //create the directory if not present
    if(!file_exists($dir_name))
        mkdir($dir_name, 0777, true);

    //calculate the destination image path
    $i_dest = $dir_name . $i_name . (($pref === false)? '' : '_' . $pref) . '.' . $img_type;

    //lets see if the path exists already
    if(file_exists($i_dest)) {
        $pref = (int) $pref;

        //modify the file name, do not overwrite
        if(false == $overwrite)
            return itg_fetch_image($img_url, $store_dir, $store_dir_type, $overwrite, ++$pref, $debug);
        //delete & overwrite
        else
            unlink ($i_dest);
    }

    //first check if the image is fetchable
    $img_info = @getimagesize($img_url);

    //is it a valid image?
    if(false == $img_info || !isset($img_info[2]) || !($img_info[2] == IMAGETYPE_JPEG || $img_info[2] == IMAGETYPE_PNG || $img_info[2] == IMAGETYPE_JPEG2000 || $img_info[2] == IMAGETYPE_GIF)) {
        if(true == $debug)
            echo 'The image doesn\'t seem to exist in the remote server';

        $arr = array('error' => True, 'message' => 'Not valid image file.');
        echo json_encode($arr);
    }

    //now try to create the image
    if($img_type == 'jpg') {
        $m_img = @imagecreatefromjpeg($img_url);
    } else if($img_type == 'png') {
        $m_img = @imagecreatefrompng($img_url);
        @imagealphablending($m_img, false);
        @imagesavealpha($m_img, true);
    } else if($img_type == 'gif') {
        $m_img = @imagecreatefromgif($img_url);
    } else {
        $m_img = FALSE;
    }

    //was the attempt successful?
    if(FALSE === $m_img) {
        if(true == $debug)
            $arr = array('error' => True, 'message' => 'Can not create image from the URL.');
            echo json_encode($arr);
        return '';
    }

    //now attempt to save the file on local server
    if($img_type == 'jpg') {
        if(imagejpeg($m_img, $i_dest, 100))
            return $i_dest;
        else
            $arr = array('error' => True, 'message' => 'File can not be saved.');
            echo json_encode($arr);
    } else if($img_type == 'png') {
        if(imagepng($m_img, $i_dest, 0))
            return $i_dest;
        else
            $arr = array('error' => True, 'message' => 'File can not be saved.');
            echo json_encode($arr);
    } else if($img_type == 'gif') {
        if(imagegif($m_img, $i_dest))
            return $i_dest;
        else
            $arr = array('error' => True, 'message' => 'File can not be saved.');
            echo json_encode($arr);
    }

    return '';
}


$url = $_POST["url"];
$newUrl = itg_fetch_image($url);

if($newUrl != '') {
  echo json_encode(array('url' => $newUrl));
}