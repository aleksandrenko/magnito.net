<?php
//don't change it or order will brake
$path = "uploads/";

	$valid_formats = array("jpg", "png", "gif", "bmp", "JPG", "PNG", "GIF");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
			$name = $_FILES['photoimg']['name'];

			$size = $_FILES['photoimg']['size'];
			$max_size = 6.5; //MB
			$mb = 1024 * 1024;
			
			if(strlen($name)) {
                $pi = pathinfo($name);
                $txt = $pi['filename'];
                $ext = $pi['extension'];

                if(in_array($ext, $valid_formats)) {
                    if($size< ($max_size * $mb)) {
                        $actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];

                        if(move_uploaded_file($tmp, $path.$actual_image_name)) {
                            $local = "uploads/" . $actual_image_name;

                            $url = array('url' => $local);
                            echo json_encode($url);

                            /*
                            $type = mime_content_type($local);
                            $allowed_types = array(
                              'image/png',
                              'image/jpeg',
                              'image/gif',
                              'image/bmp'
                            );

                            if( 0  !in_array($type, $allowed_types) ) {
                              //file is not of permited type, delete the file
                              unlink($local);

                              $arr = array('error' => True, 'message' => 'Invalid file format!');
                              echo json_encode($arr);
                            } else {
                              echo $local;
                            }*/

                        } else {
                            $arr = array('error' => True, 'message' => 'Something went wrong!');
                            echo json_encode($arr);
                        }
                    }
                    else {
                        $arr = array('error' => True, 'message' => "Image file size max " . $max_size . "MB");
                        echo json_encode($arr);
                    }
                }
                else {
                    $arr = array('error' => True, 'message' => 'Invalid file format!');
                    echo json_encode($arr);
                }
            }

			else {
				$arr = array('error' => True, 'message' => 'Invalid file format!');
        echo json_encode($arr);
		  }
				
			exit;
		}
?>