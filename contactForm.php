<?php
    $server_name = $_SERVER["SERVER_NAME"];

    if($server_name == "www.magnito.net" || $server_name == "magnito.net") {
      $name = $_POST["name"];
      $email = $_POST["email"];
      $notes = $_POST["notes"];

      if($name != '' && $email != '' && $notes != '') {
        //everything is ok send the email and return the status

        $to = 'contacts@magnito.net';
        $subject = 'hello mangnito (contact form)';
        $message = " name: $name \r\n email: $email \r\n notes: $notes";

        $headers = "From: magnito.net";

        //sending email
        $status = mail($to, $subject, $message, $headers);

        if( $status ) {
          $arr = array('error' => !$status);
        } else {
          $arr = array('error' => True, 'message' => 'Email not sended for some reason!');
        }

        echo json_encode($arr);

      } else {
        //all fields are required and must be filled
        $arr = array('error' => True, 'message' => 'All fields are required!');
        echo json_encode($arr);
      }

    } else {
      //some one is using this form and it's not ok
      $arr = array('error' => True, 'message' => 'Invalid server name!');
      echo json_encode($arr);
    }

?>
