<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Orders extends CI_Controller {

	public function index($skip = 0) {
    //if there is no session redirect to /
    if(!$this->session->userdata('logged_in')) {
      $this->load->helper('url');
      redirect('', 'refresh');
    }

    $per_page = 10;

    //get model and db entries
    $search_query = $this->input->post('search_query');
    $this->load->model('order_model', 'order_model', TRUE);
    $data['orders'] = $this->order_model->get($search_query, $per_page, $skip);


    //get all not completed orders
    $data['active_orders_count'] = $this->order_model->get_active_orders_count();


    //pagginator
    $this->load->library('pagination');

    $config['uri_segment'] = 2;

    $config['base_url'] = '/_admin/orders/';
    $config['total_rows'] = $this->order_model->count_all($search_query);
    $config['per_page'] = $per_page;
    $config['num_links'] = 10;
    $config['full_tag_open'] = '<ul class="pagination large-centered">';
    $config['full_tag_close'] = '</ul>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="current"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['prev_link'] = '&laquo;';
    $config['prev_tag_open'] = '<li class="arrow">';
    $config['prev_tag_close'] = '</li>';

    $config['next_link'] = '&raquo;';
    $config['next_tag_open'] = '<li class="arrow">';
    $config['next_tag_close'] = '</li>';


    $this->pagination->initialize($config);

    $data['links_html'] = $this->pagination->create_links();
    // end of pagginator

    //render template
    $this->load->view('list', $data);

	}
}
