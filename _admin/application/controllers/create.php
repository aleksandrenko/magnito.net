<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Create extends CI_Controller {

	public function index() {
	  //if there is no session redirect to /
    if(!$this->session->userdata('logged_in')) {
      $this->load->helper('url');
      redirect('', 'refresh');
    }

    $this->load->model('order_model', 'order_model', TRUE);
    if( $this->input->post('fullname') ) {
      $this->order_model->insert_entry();
    }


    $this->load->model('email_template_model', 'email_template_model', TRUE);
    if( $this->input->post('template') ) {
        $this->email_template_model->insert_entry();
    }


    $this->load->model('canvas_model', 'canvas_model', TRUE);
    if( $this->input->post('url') ) {
      $this->canvas_model->insert_entry();
    }


    if( $this->input->post('action') == 'copy_image_from_url') {
      $unique_name = strtotime("now") . '.jpg';

      $remote_url = $this->input->post('url');
      $local = 'upload/' . $unique_name;

      copy($remote_url, $local);

      $type = mime_content_type ($local);
      $allowed_types = array(
                              'image/png',
                              'image/jpeg',
                              'image/gif',
                              'image/bmp'
                            );

      if( !in_array($type, $allowed_types) ) {
        //file is not of permited type, delete the file
        $this->load->helper('file');
        unlink($local);
      }

      echo $type;
    }



    $this->load->view('create');
	}
}
