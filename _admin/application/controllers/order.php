<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Order extends CI_Controller {

	public function index($id=-1) {
	  //if there is no session redirect to /
    if(!$this->session->userdata('logged_in')) {
      $this->load->helper('url');
      redirect('', 'refresh');
    }

    $this->load->model('order_model', 'order_model', TRUE);

    if($this->input->post('status')) {
      $this->order_model->update_status();
    }

    if($this->input->post('admin_notes')) {
      $this->order_model->update_admin_notes();
    }

    //
    // DEFINE PRICE PER PACKAGE
    //

    $data['price_per_package'] = 19.99;
    $data['order'] = $this->order_model->get_by_id($id);


    $this->load->model('canvas_model', 'canvas_model', TRUE);
    $data['canvases'] = $this->canvas_model->get_by_order_id($data['order']->id);

    $this->load->model('email_template_model', 'email_template_model', TRUE);
    $data['email_templates'] = $this->email_template_model->get();


    if($this->input->post('action') == 'update_email_template') {
       $this->email_template_model->update_entry();
       $data['email_templates'] = $this->email_template_model->get();
    }


    $this->load->model('sended_email_model', 'sended_email_model', TRUE);

    //send email from template
    if($this->input->post('action') == 'send_mail') {
        $selected_template = '';
        $template_id = $this->input->post('template_id');

        foreach ($data['email_templates'] as $template) {
            if($template_id == $template->id) {
                $selected_template = $template;
            }
        }

        //data --------------------------------------------
        $subject = $selected_template->subject;
        $email_text = $selected_template->template;
        $email = $data['order']->email;
        $template_id = $selected_template->id;
        //-------------------------------------------------

        //
        //change email_text - change vars in the template with value
        //
        $client_fullname = $data['order']->fullname;
        $order_number = $data['order']->id;
        $client_address = $data['order']->address;
        $client_phone = $data['order']->phone;
        $client_email = $data['order']->email;
        $client_notes = $data['order']->notes;
        $total_amount = 0;
        $total_price = 0;
        $magnito_contact_email = "contacts@magnito.net";
        $delivery = 'Доставка с Еконт (за сметка на клиента)';

        foreach ($data['canvases'] as $canvas) {
          $total_amount += $canvas->amount;
          $total_price += $canvas->amount * $data['price_per_package'];
        }

        if($total_price >= 50) {
          $delivery = 'Безплатна доставка до адрес';
        }

        $total_magnito_images = $total_amount * 9;

        //
        // Change vars in email text with real value
        //
        $email_text = str_replace("{{client_fullname}}",        $client_fullname,       $email_text);
        $email_text = str_replace("{{order_number}}",           $order_number,          $email_text);
        $email_text = str_replace("{{client_email}}",           $client_email,          $email_text);
        $email_text = str_replace("{{client_address}}",         $client_address,        $email_text);
        $email_text = str_replace("{{client_phone}}",           $client_phone,          $email_text);
        $email_text = str_replace("{{client_notes}}",           $client_notes,          $email_text);
        $email_text = str_replace("{{total_amount}}",           $total_amount,          $email_text);
        $email_text = str_replace("{{total_price}}",            $total_price,           $email_text);
        $email_text = str_replace("{{delivery}}",               $delivery,              $email_text);
        $email_text = str_replace("{{magnito_contact_email}}",  $magnito_contact_email, $email_text);
        $email_text = str_replace("{{total_magnito_images}}",   $total_magnito_images,  $email_text);

        /* this will replace all spaces with html tags, in email this is not needed
        $spaces   = array("\r\n", "\n", "\r");
        $replace = '<br />';
        $email_text = str_replace($spaces, $replace, $email_text);
        */

        //send the email
        $this->load->helper('email');

        if (valid_email($email)) {
            $this->load->library('email');

            $this->email->from('contacts@magnito.net', 'Magnito.net');
            $this->email->to($email);

            $this->email->subject($subject);
            $this->email->message($email_text);

            $this->email->send();
        }
        else {
            echo 'email is not valid';
        }

        //save in db --------------------------------------
        $order_id = $data['order']->id;
        $this->sended_email_model->insert_entry($subject, $email_text, $email, $template_id, $order_id);
    }

    $order_id = $data['order']->id;
    $data['sended_emails'] = $this->sended_email_model->get($order_id);

    $data['already_sended_emails'] = array();

    foreach($data['sended_emails'] as $sended_email) {
      array_push($data['already_sended_emails'], $sended_email->template_id);
    }


    //if there is a post this code refresh the page so in page refresh there will be no post data to act on
    if($this->input->post()) {
      $this->load->helper('url');
      redirect(current_url(), 'refresh');
    }

    $this->load->view('order_view', $data);

	}
}
