<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Create_order extends CI_Controller {

	public function index() {
    $this->load->model('order_model', 'order_model', TRUE);
    $this->load->model('canvas_model', 'canvas_model', TRUE);


    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

    $this->form_validation->set_rules('fullname', 'First and Lastname', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('address', 'Delivery Address', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');

    if ($this->form_validation->run() == FALSE) {
      //not valid form
      $arr = array('error' => True, 'message' => 'Not all required fields are provided.');
      echo json_encode($arr);
    }
    else {
      //valid form
      $fullname = $this->input->post('fullname');
      $email = $this->input->post('email');
      $address = $this->input->post('address');
      $phone = $this->input->post('phone');
      $notes = $this->input->post('notes');
      $facebook = $this->input->post('facebook');
      $referrer = $this->input->post('referrer');

      $order_id = $this->order_model->create_order($fullname, $email, $address, $phone, $notes, $facebook, $referrer);

      //
      //create canvases
      //
      $packs = $this->input->post('packs');

      foreach ($packs as $pack) {
        $name = $pack["canvas_id"] . '.png';
        $uploads_dir = '../_admin/prints/';

        $from_url = '../editor/prints/' . $name;
        $to_url = $uploads_dir . $name;

        copy($from_url, $to_url) or die("Unable to copy $old to $new.");

        $url = $to_url;
        $amount = $pack["amount"];

        $this->canvas_model->create_canvas($url, $order_id, $amount);
      }


      //
      // DELETE OLD FILES IN /editor/upload and /editor/print
      // 86400 is seconds for 24h
      //

      $upload_folder = "/editor/upload/";
      foreach (glob($upload_folder."*") as $file) {
          if (filemtime($file) < time() - 86400) {
              unlink($file);
          }
      }

      $print_folder = "/editor/print/";
      foreach (glob($print_folder."*") as $file) {
          if (filemtime($file) < time() - 86400) {
              unlink($file);
          }
      }

      //
      // END DELETION
      //


      //
      // Send emails to contacts@magnito.net
      //

      $this->load->helper('email');
      $notification_email = 'contacts@magnito.net';

      $notification_email_text =  " fullname: " . $fullname . "\n email: " . $email . "\n address: " . $address . "\n phone: " . $phone . "\n notes: " . $notes . "\n facebok: " . $facebook . "\n orderid: " . $order_id . "\n\n admin page: http://www.magnito.net/_admin/order/" . $order_id;


      $this->load->library('email');

      $this->email->from('do-not-respond@magnito.net', 'admin.magnito.net');
      $this->email->to($notification_email);

      $this->email->subject('new order (' . $order_id . ')');
      $this->email->message($notification_email_text);

      $this->email->send();

      //
      // END SEND MAIL
      //


      $arr = array('error' => False, 'id' => $order_id);
      echo json_encode($arr);
    }

	}
}
