<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller {

  public function index() {
    //set session
    $newdata = array('logged_in' => FALSE);
    $this->session->set_userdata($newdata);

    $this->load->helper('url');
    redirect('/', 'refresh');

	}
}
