<?php
class Email_template_model extends CI_Model {

    var $id = '';
    var $name = '';
    var $subject = '';
    var $template = '';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get() {
        $query = $this->db->get('email_templates');
        return $query->result();
    }

    function insert_entry() {
        $this->name =      $this->input->post('name');
        $this->subject = $this->input->post('subject');
        $this->template = $this->input->post('template');

        $this->db->insert('email_templates', $this);
    }

    function update_entry() {
        $id = $this->input->post('id');
        $subject = $this->input->post('subject');
        $name = $this->input->post('name');
        $template = $this->input->post('template');

        $this->db->where('id', $id);
        $data = array(
                      'subject' => $subject,
                      'name' => $name,
                      'template' => $template
                     );

        $this->db->update('email_templates', $data);
    }

}
