<?php
class Order_model extends CI_Model {

    var $fullname = '';
    var $email = '';
    var $address = '';
    var $phone = '';
    var $notes = '';
    var $facebook = '';

    var $referrer = '';

    var $admin_notes = '';
    var $status = '';

    var $id = '';
    var $date = '';
    var $price = '';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get($search_query='', $per_page=5, $skip=0) {

        if($search_query != '') {
            $this->db->or_where('id', $search_query);
            $this->db->or_where('email', $search_query);
            $this->db->or_where('fullname', $search_query);
            $this->db->or_where('phone', $search_query);
        }

        $this->db->order_by("date", "desc");

        $query = $this->db->get('entries', $per_page, $skip);
        return $query->result();
    }

    function count_all($search_query='') {
        if($search_query != '') {
            $this->db->or_where('id', $search_query);
            $this->db->or_where('email', $search_query);
            $this->db->or_where('fullname', $search_query);
            $this->db->or_where('phone', $search_query);
        }

        $this->db->from('entries');
        return $this->db->count_all_results();
    }

    function get_by_id($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('entries', 1);
        $result = $query->result();

        $order = '';

        if(count($result) > 0) {
          $order = $result[0];
        }

        return $order;
    }

    function get_active_orders_count() {
        $this->db->where('status', '1');
        $this->db->from('entries');
        return $this->db->count_all_results();
    }

    //this one is used to get orders from the dummy ui in controllers/create.php
    function insert_entry() {
        $this->fullname = $this->input->post('fullname');
        $this->email = $this->input->post('email');
        $this->address = $this->input->post('address');

        $this->phone = $this->input->post('phone');

        $this->facebook = $this->input->post('facebook');

        $this->notes = $this->input->post('notes');
        $this->admin_notes = $this->input->post('admin_notes');
        $this->status = $this->input->post('status');

        $this->date   = date('Y-m-d H:i:s');

        $this->db->insert('entries', $this);
    }

    //this one is used to get orders from the frontend
    function create_order($fullname='', $email='', $address='', $phone='', $notes='', $facebook='', $referrer='') {
        $this->fullname = $fullname;
        $this->email = $email;
        $this->address = $address;
        $this->phone = $phone;
        $this->facebook = $facebook;
        $this->notes = $notes;
        $this->status = 1;//prieta poruchka
        $this->date   = date('Y-m-d H:i:s');
        $this->referrer = $referrer;

        $this->db->insert('entries', $this);
        return $this->db->insert_id();
    }


    function update_admin_notes() {
        $admin_notes = $this->input->post('admin_notes');
        $id = $this->input->post('id');

        $this->db->where('id', $id);
        $data = array('admin_notes' => $admin_notes);

        $this->db->update('entries', $data);
    }

    function update_status() {
        $status = $this->input->post('status');
        $id = $this->input->post('id');

        $this->db->where('id', $id);
        $data = array('status' => $status);

        $this->db->update('entries', $data);
    }
}
