<?php
class Canvas_model extends CI_Model {

    var $id = '';
    var $order_id = '';
    var $url = '';
    var $amount = '';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get_by_order_id($order_id) {

        $this->db->where('order_id', $order_id);

        $query = $this->db->get('canvases');
        return $query->result();
    }

    function insert_entry() {
        $this->url =      $this->input->post('url');
        $this->order_id = $this->input->post('order_id');
        $this->amount =   $this->input->post('amount');

        $this->db->insert('canvases', $this);
    }

    function create_canvas($url, $order_id, $amount) {
        $this->url = $url;
        $this->order_id = $order_id;
        $this->amount = $amount;

        $this->db->insert('canvases', $this);
    }

}
