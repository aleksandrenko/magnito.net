<?php
class Sended_email_model extends CI_Model {

    var $id = '';
    var $subject = '';
    var $email = '';
    var $email_text = '';
    var $date = '';
    var $template_id = '';
    var $order_id = '';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get($order_id) {
        $query = $this->db->get_where('sended_emails', array('order_id' => $order_id));

        return $query->result();
    }

    function insert_entry($subject='', $email_text='', $email='', $template_id='', $order_id='') {
        $this->subject = $subject;
        $this->email_text = $email_text;
        $this->email = $email;
        $this->template_id = $template_id;
        $this->order_id = $order_id;

        $this->date   = date('Y-m-d H:i:s');

        $this->db->insert('sended_emails', $this);
    }

}
