<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "login";
$route['logout/'] = "logout";
$route['orders/(:num)'] = "orders/index/$1";
$route['order/(:num)'] = "order/index/$1";
$route['create_order/'] = "create_order/index";
$route['create/'] = "create";
$route['404_override'] = '';
