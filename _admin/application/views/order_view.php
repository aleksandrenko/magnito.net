<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/_admin/css/style.css">
    <script src="/_admin/js/vendor/custom.modernizr.js"></script>
    <meta charset="utf-8">
</head>
<body>

<div class="topbar">
        <a href="/_admin/orders/">към всички поръчки</a>
        <span style="float: right;">administrator <a href="/_admin/logout">log out</a></span>
        <span style="float: right; margin-right: 20px" title="pass: zasada12s"><a target="order" href="https://silver.icnhost.net:2096/">contacts@magnito.net</a></span>
        &nbsp;
    </div>

    <style>
    <?php
        if($order->status == 1) {
          echo '.header { background-color: #f05537 !important; } ';
        } else if($order->status == 2) {
          echo '.header { background-color: #7ca44c !important; } ';
        } else if($order->status == 3) {
          echo '.header { background-color: #424242 !important; } ';
        }
    ?>
    </style>

    <div class="header">
        <div class="row">
            <div class="large-8 columns">
                <a href="./<?php echo $order->id; ?>"><h1>Поръчка ID #<?php echo $order->id; ?></h1></a>
                <h6><?php echo $order->date; ?>ч.</h6>
            </div>
            <div class="large-4 columns text-right">

                <?php
                    if($order->status == 1) {
                      echo '<span class="tag red preview-status" style="background-color: rgba(0,0,0,0.1)"><b>Приета поръчка</b> (изчаква изпълнение)</span>';
                    } else if($order->status == 2) {
                      echo '<span class="tag green preview-status" style="background-color: rgba(0,0,0,0.1)"><b>Изпълнена поръчка</b></span>';
                    } else if($order->status == 3) {
                      echo '<span class="tag gray preview-status" style="background-color: rgba(0,0,0,0.2)"><b>Отхвърлена поръчка</b> (клиента не е бил коректен)</span>';
                    }
                ?>

            </div>
        </div>
    </div>

    <div class="row max">
        <br />
        <div class="large-8 columns">
            <h5>Order canvases</h5>
            <div class="content-box">
                <ul class="unstyled canvases">

                    <?php $total_amount = 0; ?>
                    <?php $total_price = 0; ?>

                    <?php foreach ($canvases as $canvas):?>

                    <li>
                        <a href="/_admin/<?php echo $canvas->url ?>" target="_blank"><img src="/_admin/<?php echo $canvas->url ?>" alt="" width="200" height="200"></a>
                        <div>
                            canvas url (click to open in new tab):
                            <a href="/_admin/<?php echo $canvas->url ?>" target="_blank"><?php echo $canvas->url ?></a>
                            <br>
                            ordered count: <br><b><?php echo $canvas->amount ?></b>
                            <br><br>
                            цена: <?php echo ($canvas->amount * $price_per_package) ?> лв.

                            <?php $total_amount += $canvas->amount; ?>
                            <?php $total_price += $canvas->amount * $price_per_package; ?>
                        </div>
                    </li>

                    <?php endforeach;?>
                </ul>
            </div>

            <br>

            <h5>Order total and status</h5>
            <div class="content-box">
                <ul class="unstyled">
                    <li>
                        <div class="row collapse">
                            <span class="large-3 column">Total ordered packets:</span>
                            <span class="tag gray"><?php echo count($canvases) ?></span>
                        </div>
                    </li>
                    <li>
                        <div class="row collapse">
                            <span class="large-3 column">Total ordered amount:</span>
                            <span class="tag gray">
                              <?php echo $total_amount; ?>
                        </div>
                    </li>
                    <li>
                        <div class="row collapse">
                            <span class="large-3 column">Total Price:</span>
                            <strong class="tag gray"><?php echo $total_price ?> лв.</strong>
                        </div>
                    </li>
                </ul>

                <hr>

                <ul class="unstyled collapse">
                    <li>
                        <div class="row endstatus">
                            <form action="" method="POST">
                              <span class="large-3 column">Status:</span>
                              <span class="large-7 column">

                                <input type="hidden" name="id" value="<?php echo $order->id ?>" />

                                <select name="status">
                                  <option value="1" <?php if($order->status == 1) { echo 'selected'; }; ?> >Приета</option>
                                  <option value="2" <?php if($order->status == 2) { echo 'selected'; }; ?> >Изпълнена</option>
                                  <option value="3" <?php if($order->status == 3) { echo 'selected'; }; ?> >Отхвърлена</option>
                                </select>

                              </span>
                              <span class="large-2 column">
                                <button onclick="javascript:return confirm('Are you sure you want to change the status?')">Запиши</button>
                              </span>
                            </form>
                        </div>
                    </li>
                </ul>

                <hr>

                <ul class="unstyled collapse">
                    <li>
                        <div class="row">
                            <span class="large-3 column">Emails:</span>
                            <span class="large-9 columns">

                                <?php foreach ($sended_emails as $sended_email): ?>
                                    <div class="row emailrow">
                                        <span class="large-5 columns">
                                            <b><a href="#" data-reveal-id="email_template_<?php echo $sended_email->template_id; ?>">
                                                <?php
                                                    foreach ($email_templates as $email_template) {
                                                        if($email_template->id == $sended_email->template_id) {
                                                            echo $email_template->name;
                                                        }
                                                    }
                                                ?>
                                            </a></b>
                                        </span>
                                        <span class="large-3 columns">
                                            <?php echo $sended_email->date; ?>
                                        </span>
                                        <span class="large-4 columns text-right">
                                            Изпратен
                                        </span>
                                    </div>

                                <?php endforeach;?>


                                <!-- print data from templates only -->

                                <?php foreach ($email_templates as $email_template):?>

                                    <!-- check if the template is not used -->

                                    <?php if (!in_array($email_template->id, $already_sended_emails)): ?>
                                        <div class="row emailrow">
                                            <span class="large-5 columns">
                                                <b><a href="#" data-reveal-id="email_template_<?php echo $email_template->id ?>"><?php echo $email_template->name; ?></a></b>
                                            </span>
                                            <span class="large-3 columns">
                                                -
                                            </span>
                                            <span class="large-4 columns text-right">
                                                <form action="" method="POST" style="margin: 0">
                                                    <input type="hidden" value="send_mail" name="action" />
                                                    <input type="hidden" value="<?php echo $email_template->id ?>" name="template_id" />
                                                    <button class="small" onclick="javascript:return confirm('Are you sure you want to send this email?')">
                                                        Изпрати
                                                    </button>
                                                </form>
                                            </span>
                                        </div>

                                    <?php endif; ?>

                                <?php endforeach;?>

                            </span>
                        </div>
                    </li>
                </ul>

            </div>

        </div>



        <div class="large-4 columns">
            <h5>Personal info and notes</h5>
            <div class="content-box">
                <div class="row">
                    <div class="large-2 columns">

                        <?php
                          if($order->facebook != '') {
                            echo '<img src="https://graph.facebook.com/' . $order->facebook . '/picture" alt="" width="50" height="50" />';
                          } else {
                            echo '<img src="/_admin/img/sample-avatar.jpg" alt="" width="50" height="50" />';
                          }
                        ?>

                    </div>
                    <div class="large-10 columns">
                        <ul class="unstyled">
                            <li>
                                <h3 class="collapse"><a href=""><?php echo $order->fullname; ?></a></h3>
                                <?php echo $order->email; ?>
                            </li>
                            <li>
                                <?php echo $order->phone; ?><br>
                                <a href="http://www.facebook.com/<?php echo $order->facebook; ?>"><?php echo $order->facebook; ?></a>
                            </li>
                            <li>
                                <?php echo $order->address; ?>
                            </li>
                            <li>
                                <?php echo $order->notes; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="content-box">
                <h6>Notes:</h6>
                <form action="" method="POST">
                  <input type="hidden" name="id" value="<?php echo $order->id; ?>" />
                  <textarea name="admin_notes" id="" cols="30" rows="20" style="height: 150px"><?php echo $order->admin_notes; ?></textarea>
                  <button class="small">Save Notes</button>
                </form>
            </div>
        </div>
    </div>



    <?php foreach ($email_templates as $email_template):?>

        <div id="email_template_<?php echo $email_template->id ?>" class="reveal-modal">
            <a class="close-reveal-modal">&#215;</a>
            <h5><?php echo $email_template->name; ?> код:</h5>
            <p>
                <?php
                    foreach ($sended_emails as $sended_email) {
                        if($sended_email->template_id == $email_template->id) {
                            echo $sended_email->email_text;
                        }
                    }
                ?>
            </p>

            <hr>

            <form action="" method="POST">
                <input type="hidden" name="action" value="update_email_template" />
                <input type="hidden" name="id" value="<?php echo $email_template->id; ?>" />

                <div class="row" style="max-width: 100%;">
                    <div class="large-6 columns" style="padding-left: 0px;">
                        Template name:<br />
                        <input type="text" name="name" value="<?php echo $email_template->name; ?>" /><br/>
                    </div>
                    <div class="large-6 columns" style="padding-right: 0px;">
                        Subject:<br />
                        <input type="text" name="subject" value="<?php echo $email_template->subject; ?>" />
                    </div>
                </div>

                <textarea name="template" cols="30" rows="10" style="height: 270px"><?php echo $email_template->template; ?></textarea>
                <input type="submit" value="Запиши шаблона" />
            </form>
            <p>
                $client_fullname,
                $order_number,
                $client_address,
                $client_email,
                $client_phone,
                $client_notes,
                $total_amount,
                $total_price,
                $magnito_contact_email,
                $total_magnito_images,
                $delivery
            </p>
        </div>
    <?php endforeach;?>


    <script type="text/javascript" src="/_admin/js/vendor/jquery.js"></script>
    <script type="text/javascript" src="/_admin/js/foundation/foundation.js"></script>
    <script type="text/javascript" src="/_admin/js/foundation/foundation.reveal.js"></script>

    <script type="text/javascript">
        $(document).foundation();
    </script>
</body>


</body>
</html>