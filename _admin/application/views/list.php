<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/_admin/css/style.css">
    <script src="/_admin/js/vendor/custom.modernizr.js"></script>
    <meta charset="utf-8">
</head>
<body>

    <div class="topbar">
        <span style="float: right;">administrator <a href="/_admin/logout">log out</a></span>
        <span style="float: right; margin-right: 20px" title="pass: zasada12s"><a target="order" href="https://silver.icnhost.net:2096/">contacts@magnito.net</a></span>
        &nbsp;
    </div>

    <div class="header">
        <div class="row">
            <div class="large-8 columns">
                <h1>Magnito orders (<?php echo $active_orders_count; ?> new)</h1>
            </div>
            <div class="large-4 columns text-right">
                <form action="" method="POST">
                    <div class="row collapse">
                        <div class="large-10 columns">
                            <input type="text" name="search_query" placeholder="order id, date or customer name" value="<?php echo $this->input->post('search_query') ?>" />
                        </div>
                        <div class="large-2 columns">
                           <input type="submit" value="Търси" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="content-wrapper">
        <table>
            <thead>
                <tr>
                    <th>order</th>
                    <th width="110">facebook</th>
                    <th width="150">phone</th>
                    <th width="180">order date</th>
                    <th width="40">price</th>
                    <th>referrer</th>
                    <th width="20">ID</th>
                    <th width="120">status</th>
                    <th width="50">more</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($orders as $order):?>
                    <tr>
                        <td class="order">
                            <?php
                              if($order->facebook != '') {
                                echo '<img src="https://graph.facebook.com/' . $order->facebook . '/picture" alt="" width="50" height="50" />';
                              } else {
                                echo '<img src="/_admin/img/sample-avatar.jpg" alt="" width="50" height="50" />';
                              }
                            ?>
                            <a href="/_admin/order/<?php echo $order->id; ?>"><?php echo $order->fullname;?></a>
                            <?php echo $order->email;?>
                        </td>
                        <td>
                            <a href="http://www.facebook.com/<?php echo $order->facebook; ?>" target="_fb"><?php echo $order->facebook; ?></a>
                        </td>
                        <td>
                            <?php echo $order->phone;?>
                        </td>
                        <td>
                            <?php echo $order->date; ?>
                        </td>
                        <td>
                            <?php echo $order->price; ?>
                        </td>
                        <td style="max-width: 200px; text-overflow: ellipsis; overflow: hidden;" title="<?php echo $order->referrer ? $order->referrer : '-' ?>">
                            <?php echo $order->referrer ? $order->referrer : '-' ?>
                        </td>
                        <td>
                            <?php echo $order->id;?>
                        </td>
                        <td>
                            <?php
                              if($order->status == 1) {
                                echo '<span class="tag red">Приета</span>';
                              } else if($order->status == 2) {
                                echo '<span class="tag green">Изпълнена</span>';
                              } else if($order->status == 3) {
                                echo '<span class="tag gray">Отхвърлена</span>';
                              }
                            ?>

                        </td>
                        <td>
                            <a href="/_admin/order/<?php echo $order->id; ?>" class="button">More</a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>

        <?php echo $links_html; ?>

        <!--
        <ul class="pagination large-centered">
            <li class="arrow unavailable"><a href="">&laquo;</a></li>
            <li class="current"><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
            <li class="unavailable"><a href="">&hellip;</a></li>
            <li><a href="">12</a></li>
            <li><a href="">13</a></li>
            <li class="arrow"><a href="">&raquo;</a></li>
        </ul>
        -->

    </div>

</body>
</html>