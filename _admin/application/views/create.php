<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
</head>
<body>
  <h1>Create order</h1>

  <style>
    label {
      display: inline-block;
      width: 100px;
    }

    input, textarea {
      width: 200px;
    }

  </style>

  <form method="POST" action="">
    <label>Full name:</label>
    <input type="text" name="fullname" />

    <br /><br />

    <label>Email:</label>
    <input type="text" name="email" />

    <br /><br />

    <label>Address:</label>
    <input type="text" name="address" />

    <br /><br />

    <label>Phone:</label>
    <input type="text" name="phone" />

    <br /><br />

    <label>Facebook:</label>
    <input type="text" name="facebook" />

    <br /><br />

    <label>Notes:</label>
    <textarea name="notes"></textarea>

    <br /><br />

    <label>Admin notes:</label>
    <textarea name="admin_notes"></textarea>

    <br /><br />

    <label>Status</label>
    <select name="status">
      <option value="1">Приета</option>
      <option value="2">Завършена</option>
      <option value="3">Отхвърлена</option>
    </select>

    <br /><br />

    <input type="submit" />

  </form>


  <h1>Canvas</h1>

  <form action="" method="POST">

    <label>URL:</label>
    <input type="text" name="url" />

    <br /><br />

    <label>Order id:</label>
    <input type="text" name="order_id" />

    <br /><br />

    <label>Amount:</label>
    <select name="amount">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
    </select>

    <br /><br />

    <input type="submit" />

  </form>


  <br><br>
  <hr />

  <h1>Email templates</h1>
  <form action="" method="POST" >
    <label>Name:</label>
    <input type="text" name="name" />

    <br /><br />

    <label>Subject:</label>
    <input type="text" name="subject" />

    <br /><br />

    <label>Template:</label>
    <textarea name="template" style="width: 450px; height: 100px;"></textarea>

    <br /><br />

    <input type="submit" />
  </form>


  <br><br><hr>

  <h1>Copy image from url</h1>
  <form action="" method="POST">
    <input type="hidden" name="action" value="copy_image_from_url" />
    <input type="text" name="url" value="http://media-cache-lt0.pinterest.com/550x/e6/bd/82/e6bd823f49ce00ed51a4da2761e81f6e.jpg" /><br />
    <input type="submit" />
  </form>


  <br><br><hr>

  <h1>Upload image</h1>
  <form action="" method="POST">
    <input type="hidden" name="action" value="upload_image" />
    <input type="file" name="file" /><br />
    <input type="submit" />
  </form>

</body>
</html>