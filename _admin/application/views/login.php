<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/_admin/css/style.css">
    <script src="/_admin/js/vendor/custom.modernizr.js"></script>
    <meta charset="utf-8">
</head>
<body>

    <div class="login">
        <div class="panel">
            <form action="./" method="POST">
                <h5>ADMIN LOGIN</h5>
                <div class="content">
                    User: <input autofocus="autofocus" type="text" name="username" placeholder="username" value="<?php echo $this->input->post('username'); ?>" />
                    Password: <input type="password" name="password" placeholder="password"  value="<?php echo $this->input->post('password'); ?>" />
                </div>
                <div class="footer">
                    <input type="submit" value="Login" class="blue" />
                </div>
            </form>
        </div>
    </div>
</body>
</html>
